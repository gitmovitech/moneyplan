import React, { Component } from 'react';
import Resumen from './components/resumen';
import { Tabs, Tab, Button } from 'react-bootstrap';
import Tabla from './components/tabla';
import ModalWindow from './components/modal';

class App extends Component {

  constructor(props) {
    super(props);

    this.cantidadDias = new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0);
    this.cantidadDias = Math.ceil((this.cantidadDias.getTime() - new Date().getTime()) / (1000 * 60 * 60 * 24));
    this.balance = 0;
    this.gastos = 0;
    this.entradas = 0;

    this.state = {
      balance: this.balance,
      gastos: this.gastos,
      entradas: this.entradas,
      diario: Math.round(this.balance / this.cantidadDias),
      statusModalOpenGasto: false,
      statusModalOpenEntrada: false
    }

    this.OpenModalGasto = this.OpenModalGasto.bind(this);
    this.CloseModalGasto = this.CloseModalGasto.bind(this);
    this.OpenModalEntrada = this.OpenModalEntrada.bind(this);
    this.CloseModalEntrada = this.CloseModalEntrada.bind(this);
    this.RefreshResume = this.RefreshResume.bind(this);
    this.Remove = this.Remove.bind(this);

  }

  componentDidMount() {
    this.RefreshResume();
  }

  RefreshResume() {
    let data = [];
    try {
      data = JSON.parse(localStorage.moneyplandb);
    } catch (e) {
      data = [];
    }
    this.balance = 0;
    this.gastos = 0;
    this.entradas = 0;

    data.forEach(item => {
      if (item.type === 'in') {
        this.balance = this.balance + parseInt(item.monto);
        this.entradas += parseInt(item.monto);
      } else {
        this.balance = this.balance - parseInt(item.monto);
        this.gastos += parseInt(item.monto)
      }
    });
    this.setState(state => ({
      balance: this.balance,
      gastos: this.gastos,
      entradas: this.entradas,
      diario: Math.round(this.balance / this.cantidadDias)
    }));
  }

  Remove(item) {
    if (window.confirm('¿Deseas remover "'+item.titulo+'" por $'+item.monto+' del listado?')) {
      let data = [];
      try {
        data = JSON.parse(localStorage.moneyplandb);
      } catch (e) { }

      data.forEach((single, n) => {
        if (item.monto === single.monto && item.titulo === single.titulo && item.fecha === single.fecha) {
          data.splice(n, 1);
        }
      });
      localStorage.moneyplandb = JSON.stringify(data);

      this.RefreshResume();
    }
  }

  OpenModalEntrada() {
    this.setState(state => ({
      statusModalOpenEntrada: true
    }));
  }

  CloseModalEntrada() {
    this.setState(state => ({
      statusModalOpenEntrada: false
    }));
    this.RefreshResume();
  }

  OpenModalGasto() {
    this.setState(state => ({
      statusModalOpenGasto: true
    }));
  }

  CloseModalGasto() {
    this.setState(state => ({
      statusModalOpenGasto: false
    }));
    this.RefreshResume();
  }

  render() {
    return (
      <div className="App">

        <ModalWindow title="Ingresar nuevo gasto" open={this.state.statusModalOpenGasto} close={this.CloseModalGasto} type="out" />
        <ModalWindow title="Ingresar nueva entrada" open={this.state.statusModalOpenEntrada} close={this.CloseModalEntrada} type="in" />

        <Resumen balance={this.state.balance} gastos={this.state.gastos} entradas={this.state.entradas} diario={this.state.diario} />

        <Tabs defaultActiveKey={1} id="tabs">
          <Tab eventKey={1} title="Gastos">
            <div className="padding">
              <Button bsStyle="primary" bsSize="xsmall" onClick={this.OpenModalGasto}>Agregar Gasto</Button>
            </div>
            <Tabla type="out" remove={this.Remove} />
          </Tab>
          <Tab eventKey={2} title="Entradas">
            <div className="padding">
              <Button bsStyle="success" bsSize="xsmall" onClick={this.OpenModalEntrada}>Agregar Entrada</Button>
            </div>
            <Tabla type="in" remove={this.Remove} />
          </Tab>
        </Tabs>
      </div>
    );
  }
}

export default App;

import React, { Component } from 'react';
import { Table, Button } from 'react-bootstrap';
import propTypes from 'prop-types';
import CurrencyFormat from 'react-currency-format';

class Tabla extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: []
    }

    this.GetNumberZeros = this.GetNumberZeros.bind(this);
    this.FormatSpanishDate = this.FormatSpanishDate.bind(this);
  }

  static propTypes = {
    type: propTypes.string.isRequired,
    remove: propTypes.func.isRequired
  }

  GetNumberZeros(number) {
    return ("0" + number).slice(-2);
  }

  FormatSpanishDate(fecha){
    return [
      [this.GetNumberZeros(fecha.getDate()), this.GetNumberZeros(fecha.getMonth() + 1), fecha.getFullYear()].join('/'),
      [this.GetNumberZeros(fecha.getHours()), this.GetNumberZeros(fecha.getMinutes())].join(':')
    ].join(' ');
  }

  render() {

    let data = [];
    try {
      data = JSON.parse(localStorage.moneyplandb);
    } catch (e) {
      data = [];
    }

    let items = [];
    data.forEach(item => {
      if (item.type === this.props.type) {
        items.push(item);
      }
    });
    this.state.data = items.reverse()

    const { remove } = this.props;

    return (
      <div className="padding">
        <Table striped bordered condensed hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Título</th>
              <th>Cantidad</th>
              <th>Fecha</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.state.data.map((item, n) => {
              let fecha = new Date(item.fecha);
              fecha = this.FormatSpanishDate(fecha);
              return <tr key={n}>
                <td>{n + 1}</td>
                <td>{item.titulo}</td>
                <td><CurrencyFormat value={item.monto} displayType={'text'} thousandSeparator={true} prefix={'$'} /></td>
                <td>{fecha}</td>
                <td><Button id={n} onClick={() => remove(item)} bsStyle="danger" bsSize="xsmall">x</Button></td>
              </tr>
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default Tabla;

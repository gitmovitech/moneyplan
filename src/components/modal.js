import React, { Component } from 'react';
import propTypes from 'prop-types';
import { Modal, Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

class ModalWindow extends Component {

    constructor(props) {
        super(props);

        this.state = {
            titulo: '',
            monto: ''
        }

        this.data = [];

        this.handleChangeMonto = this.handleChangeMonto.bind(this);
        this.handleChangeTitulo = this.handleChangeTitulo.bind(this);
        this.handleGuardar = this.handleGuardar.bind(this);

    }

    static propTypes = {
        title: propTypes.string.isRequired,
        open: propTypes.bool.isRequired,
        close: propTypes.func.isRequired,
        type: propTypes.string.isRequired
    }

    handleChangeMonto(e) {
        this.setState({ monto: e.target.value });
    }

    handleChangeTitulo(e) {
        this.setState({ titulo: e.target.value });
    }

    handleGuardar() {
        if (this.state.monto > 0 && this.state.titulo !== '') {
            try {
                this.data = JSON.parse(localStorage.moneyplandb);
            } catch (e) {
                this.data = [];
            }
            this.data.push({
                monto: this.state.monto,
                titulo: this.state.titulo,
                fecha: new Date().getTime(),
                type: this.props.type
            });
            localStorage.moneyplandb = JSON.stringify(this.data);
            this.setState({ monto: '', titulo: '' });
            this.props.close();
        }
    }

    render() {
        const { title, open, close } = this.props;
        return (
            <Modal show={open} onHide={close}>
                <Modal.Header closeButton>
                    <Modal.Title>{title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <FormGroup>
                        <ControlLabel>Monto</ControlLabel>
                        <FormControl
                            type="number"
                            pattern="\d*"
                            value={this.state.monto}
                            placeholder="Ingresa el monto"
                            onChange={this.handleChangeMonto} />
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Título</ControlLabel>
                        <FormControl
                            type="text"
                            value={this.state.titulo}
                            placeholder="Descripción del monto"
                            onChange={this.handleChangeTitulo} />
                    </FormGroup>
                </Modal.Body>
                <Modal.Footer>
                    <Button bsStyle="warning" onClick={this.handleGuardar}>Guardar</Button>
                    <Button onClick={close}>Cancelar</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default ModalWindow;

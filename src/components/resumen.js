import React, { Component } from 'react';
import { Jumbotron } from 'react-bootstrap';
import propTypes from 'prop-types';
import CurrencyFormat from 'react-currency-format';

class Resumen extends Component {

    static propTypes = {
        balance: propTypes.number,
        gastos: propTypes.number,
        entradas: propTypes.number,
        diario: propTypes.number
    }

    render() {
        const { balance, gastos, entradas, diario } = this.props;
        return (
            <Jumbotron>
                <div className="container">
                    <small>V1.0</small>
                    <h1 className="text-center"><CurrencyFormat value={balance} displayType={'text'} thousandSeparator={true} prefix={'$'} /></h1>
                    <h5>Total Gastos: <CurrencyFormat value={gastos} displayType={'text'} thousandSeparator={true} prefix={'$'} /></h5>
                    <h5>Total Entradas: <CurrencyFormat value={entradas} displayType={'text'} thousandSeparator={true} prefix={'$'} /></h5>
                    <h5>Débito por día: <CurrencyFormat value={diario} displayType={'text'} thousandSeparator={true} prefix={'$'} /></h5>
                </div>
            </Jumbotron>
        );
    }
}

export default Resumen;
